// creating variables for the elements we need to interact with.
const todoInput = document.querySelector('.new-todo');
const todoList = document.querySelector('.todo-list');
const todoCount = document.querySelector('.todo-count');
const filters = document.querySelectorAll('.filters a');
const clearCompletedBtn = document.querySelector('.clear-completed');
const toggleAll = document.getElementById('toggle-all');

// creating an array of todos and setting the current filter to 'all'
let todos = JSON.parse(localStorage.getItem('todos')) || [];
let currentFilter = 'all';

// function to save todos to local storage
function saveTodos() {
    localStorage.setItem('todos', JSON.stringify(todos));
}

// function to add a todo
function addTodo(e) {
    if (e.key !== 'Enter' || !todoInput.value) return;
    const todo = {
        id: Date.now(),
        title: todoInput.value.trim(),
        completed: false,
    }
    todos.push(todo);
    todoInput.value = '';
    saveTodos();
    render();
}

// function to delete a todo
function deleteTodo(e) {
    // console.log(e.target.closest('li').dataset.id);
    const todoId = parseInt(e.target.closest('li').dataset.id);
    todos = todos.filter(todo => todo.id !== todoId);
    saveTodos();
    render();
}

// function to toggle a todo
function toggleTodo(e) {
    const todoId = parseInt(e.target.closest('li').dataset.id);
    todos = todos.map(todo => {
        if (todo.id === todoId) {
            todo.completed = !todo.completed;
        }
        return todo;
    });
    saveTodos();
    render();
}

// function to start editing a todo
function startEditing(e) {
    const todoItem = e.target.closest('li');
    todoItem.classList.add('editing');
    todoItem.querySelector('.edit').focus();
}

// function to edit a todo
function editTodo(e) {
    if (e.key !== 'Enter' && e.key !== 'Escape') return;
    const todoId = parseInt(e.target.closest('li').dataset.id);
    const newTitle = e.target.value.trim();
    todos = todos.map(todo => {
        if (todo.id === todoId) {
            if (e.key === 'Enter' && newTitle) {
                todo.title = newTitle;
            }
            todo.editing = false;
        }
        return todo;
    });
    saveTodos();
    render();
}

// function to toggle all todos
function toggleAllTodos(e) {
    todos = todos.map(todo => {
        todo.completed = e.target.checked;
        return todo;
    });
    saveTodos();
    render();
}

// function to clear completed todos
function clearCompletedTodos() {
    todos = todos.filter(todo => !todo.completed);
    saveTodos();
    render();
}

// function to update the filter
function updateFilter(e) {
    e.preventDefault();
    filters.forEach(filter => filter.classList.remove('selected'));
    e.target.classList.add('selected');
    // console.log(e.target.hash.slice(2))
    currentFilter = e.target.hash.slice(2);
    render();
}

// function to render the todos
function render() {

    // filtering the todos
    const filteredTodos = todos.filter(todo => {
        if (currentFilter === 'active') return !todo.completed;
        if (currentFilter === 'completed') return todo.completed;
        return true;
    });
    // console.log(filteredTodos)

    // creating the todo items
    const todoItems = filteredTodos.map((todo) => `
        <li data-id="${todo.id}" class="${todo.completed ? 'completed' : ''}">
            <div class="view">
                <input class="toggle" type="checkbox" ${todo.completed ? 'checked' : ''}>
                <label>${todo.title}</label>
                <button class="destroy"></button>
            </div>
            <input class="edit" value="${todo.title}">
        </li>
    `).join('');
    // console.log(todoItems)

    // rendering the todos
    todoList.innerHTML = todoItems;
    // rendering the todo count
    todoCount.textContent = todos.filter(todo => !todo.completed).length + ' items left';
    
    // event listeners
    // console.log(todoList)
    // console.log(todoList.querySelectorAll('label'))
    // console.log(todoList.querySelectorAll('.edit'))
    // console.log(todoList.querySelectorAll('.destroy'))
    // console.log(todoList.querySelectorAll('.toggle'))
    todoList.querySelectorAll('.toggle').forEach(toggle => toggle.addEventListener('change', toggleTodo));
    todoList.querySelectorAll('label').forEach(label => label.addEventListener('dblclick', startEditing));
    todoList.querySelectorAll('.edit').forEach(edit => edit.addEventListener('keydown', editTodo));
    todoList.querySelectorAll('.destroy').forEach(destroy => destroy.addEventListener('click', deleteTodo));
    // console.log(clearCompletedBtn)
    clearCompletedBtn.addEventListener('click', clearCompletedTodos);

    // toggle all
    // console.log(todos.every(todo => todo.completed))
    if (todos.every(todo => todo.completed)) {
        toggleAll.checked = true;
    } else {
        toggleAll.checked = false;
    }
}

// event listeners
// console.log(todoInput)
todoInput.addEventListener('keydown', addTodo);
toggleAll.addEventListener('change', toggleAllTodos);
filters.forEach(filter => filter.addEventListener('click', updateFilter));
// console.log(filters)

// Calling the render function
render();